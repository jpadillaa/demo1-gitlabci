import csv

# Definir la lista que contendrá los datos de las ciudades
ciudades = []

# Leer el archivo CSV y guardar los datos en la lista
with open('ciudades.csv', newline='', encoding='utf-8') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        ciudades.append(row)

# Ordenar la lista de ciudades por cantidad de habitantes (de mayor a menor)
ciudades_ordenadas = sorted(ciudades, key=lambda x: int(x['habitantes']), reverse=True)

# Imprimir las ciudades en orden descendente de acuerdo a la cantidad de habitantes
print("Ciudades ordenadas por cantidad de habitantes:")
for ciudad in ciudades_ordenadas:
    print(ciudad['ciudad'])
